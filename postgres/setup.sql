CREATE TABLE review (
  id SERIAL PRIMARY KEY,
  create_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  review VARCHAR (255),
  email VARCHAR (100),
  name VARCHAR (100)
);